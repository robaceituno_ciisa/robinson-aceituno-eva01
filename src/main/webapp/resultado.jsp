<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
              integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
        <link rel="stylesheet" href="css/style.css"/>
        <title>Calculadora de Interes</title>
    </head>
    <body>
        <nav class="navbar navbar-expand-md navbar-dark bg-dark">

            <div class="mx-auto order-0">
                <a class="navbar-brand mx-auto" href="#"><h1>Resultado : Calculo de Interes Simple</h1></a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target=".dual-collapse2">
                    <span class="navbar-toggler-icon"></span>
                </button>
            </div>
        </nav>
        <br>
        <div class="container-fluid h-100">
            <div class="row justify-content-center align-items-center h-100">
                <div class="col col-sm-6 col-md-6 col-lg-4 col-xl-3">
                    <form name="form" action="controller" method="POST" >
                        <div class="form-group">
                            <label>Monto</label>
                            <input class="form-control form-control-lg" type="number" name="monto" min="0" onkeypress="return isNumber(event)" onpaste="return false;">
                        </div>
                        <div class="form-group">
                            <label>Tasa</label>
                            <input class="form-control form-control-lg" type="number" name="tasa" min="0" max="100" onkeypress="return isNumber(event)" onpaste="return false;">
                        </div>
                        <div class="form-group">
                            <label>Años</label>
                            <input class="form-control form-control-lg" type="number" name="agnos" min="0" max="30" onkeypress="return isNumber(event)" onpaste="return false;">
                        </div>
                    </form>
                </div>
            </div>
        </div>s
        <footer class="fixed-bottom">
            <div class="container-fluid text-center bg-dark text-white">
                <span>Taller Aplicaciones Empresariales - UNI01 - CIISA 2020 ::: Robinson Aceituno</span>
            </div>
        </footer>
    </body>
</html>
